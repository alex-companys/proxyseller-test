package com.proxy.proxyseller.repository;

import com.proxy.proxyseller.model.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CommentRepository extends MongoRepository<Comment, String> {
}
