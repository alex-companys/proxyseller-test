package com.proxy.proxyseller.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Entity representing a post in the application.
 * It includes details such as the content of the post,
 * the ID of the user who created it, timestamps,
 * and lists of likes and comments.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@Document
public class Post {
    @Id
    private String id;
    private String userId;
    private String content;
    private LocalDateTime createdAt;
    private List<String> likes;
    private List<Comment> comments;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return id.equals(post.id) && userId.equals(post.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId);
    }
}
