package com.proxy.proxyseller.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Entity representing a comment on a post in the application.
 * It includes information such as the comment content,
 * the ID of the post it belongs to, the ID of the user who made the comment,
 * and the timestamp when the comment was created.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@Document
public class Comment {
    @Id
    private String id;
    private String postId;
    private String userId;
    private String content;
    private LocalDateTime createdAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id.equals(comment.id) && postId.equals(comment.postId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, postId);
    }
}
