package com.proxy.proxyseller.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Objects;

/**
 * Entity representing a user in the application.
 * This class stores user-related information like username,
 * email, password hash, and the list of users they are following.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class User {

    private String id;
    private String username;
    private String email;
    private String passwordHash;
    private List<String> following;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id) && username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username);
    }
}
