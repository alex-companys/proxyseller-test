package com.proxy.proxyseller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProxySellerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProxySellerApplication.class, args);
        System.out.println("Hello from Proxy-Seller");
    }

}
