package com.proxy.proxyseller.controller;

import com.proxy.proxyseller.model.Comment;
import com.proxy.proxyseller.model.Post;
import com.proxy.proxyseller.service.PostService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/feed")
public class PostController {

    private final PostService postService;

    @PostMapping
    public Post createPost(@RequestBody Post post) {
        return postService.createPost(post);
    }

    @PutMapping("/{postId}")
    public Post editPost(@PathVariable String postId, @RequestBody Post post) {
        return postService.editPost(postId, post);
    }

    @DeleteMapping("/{postId}")
    public void deletePost(@PathVariable String postId) {
        postService.deletePost(postId);
    }


    @PostMapping("/{postId}/like/{userId}")
    public void likePost(@PathVariable String postId, @PathVariable String userId) {
        postService.likePost(postId, userId);
    }

    @DeleteMapping("/{postId}/unlike/{userId}")
    public void unlikePost(@PathVariable String postId, @PathVariable String userId) {
        postService.unlikePost(postId, userId);
    }

    @PostMapping("/{postId}/comments")
    public Comment addComment(@PathVariable String postId, @RequestBody Comment comment) {
        return postService.addComment(postId, comment);
    }

    @DeleteMapping("/{postId}/comments/{commentId}")
    public void deleteComment(@PathVariable String postId, @PathVariable String commentId) {
        postService.deleteComment(postId, commentId);
    }
}

