package com.proxy.proxyseller.controller;

import com.proxy.proxyseller.model.User;
import com.proxy.proxyseller.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public User registerUser(@RequestBody User user) {
        return userService.register(user);
    }

    @PutMapping("/{userId}")
    public User editUser(@PathVariable String userId, @RequestBody User user) {
        return userService.editUser(userId, user);
    }

    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable String userId) {
        userService.deleteUser(userId);
    }

    @PostMapping("/{userId}/follow/{targetUserId}")
    public void followUser(@PathVariable String userId, @PathVariable String targetUserId) {
        userService.followUser(userId, targetUserId);
    }

    @PostMapping("/{userId}/unfollow/{targetUserId}")
    public void unfollowUser(@PathVariable String userId, @PathVariable String targetUserId) {
        userService.unfollowUser(userId, targetUserId);
    }
}
