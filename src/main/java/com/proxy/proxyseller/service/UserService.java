package com.proxy.proxyseller.service;

import com.proxy.proxyseller.model.User;
import com.proxy.proxyseller.repository.UserRepository;
import org.springframework.stereotype.Service;

/**
 * Service class for handling user-related operations.
 * It provides functionality for user registration, editing user details,
 * deleting users, and managing user follow/unfollow actions.
 */
@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User register(User user) {
        validateUser(user);
        userRepository.findByUsernameOrEmail(user.getUsername(), user.getEmail())
                .ifPresent(u -> {
                    throw new IllegalArgumentException("User with given username or email already exists");
                });
        return userRepository.save(user);
    }

    public User editUser(String userId, User updatedUser) {
        return userRepository.findById(userId).map(user -> {
            if (updatedUser.getUsername() != null) user.setUsername(updatedUser.getUsername());
            if (updatedUser.getEmail() != null) user.setEmail(updatedUser.getEmail());
            return userRepository.save(user);
        }).orElseThrow(() -> new IllegalArgumentException("User not found"));
    }

    public void deleteUser(String userId) {
        if (!userRepository.existsById(userId)) {
            throw new IllegalArgumentException("User not found");
        }
        userRepository.deleteById(userId);
    }

    public void followUser(String userId, String targetUserId) {
        if (userId.equals(targetUserId)) {
            throw new IllegalArgumentException("User cannot follow themselves");
        }
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new IllegalArgumentException("User not found"));
        User targetUser = userRepository.findById(targetUserId)
                .orElseThrow(() -> new IllegalArgumentException("Target user not found"));

        user.getFollowing().add(targetUserId);
        userRepository.save(user);
    }

    public void unfollowUser(String userId, String targetUserId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new IllegalArgumentException("User not found"));
        if (!user.getFollowing().remove(targetUserId)) {
            throw new IllegalStateException("User is not following the target user");
        }
        userRepository.save(user);
    }

    private void validateUser(User user) {
        if (user.getUsername() == null || user.getUsername().isEmpty()) {
            throw new IllegalArgumentException("Username cannot be empty");
        }
        if (user.getEmail() == null || user.getEmail().isEmpty()) {
            throw new IllegalArgumentException("Email cannot be empty");
        }

    }
}
