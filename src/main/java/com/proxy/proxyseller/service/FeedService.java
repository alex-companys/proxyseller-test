package com.proxy.proxyseller.service;

import com.proxy.proxyseller.model.Post;
import com.proxy.proxyseller.model.User;
import com.proxy.proxyseller.repository.PostRepository;
import com.proxy.proxyseller.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class for handling feed-related operations.
 * This includes aggregating and retrieving posts for user feeds,
 * both for the logged-in user and for other users' feeds.
 * The feed consists of posts from users that the current user follows.
 */
@Service
public class FeedService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public FeedService(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    public List<Post> getUserFeed(String userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new IllegalArgumentException("User not found"));
        List<String> following = user.getFollowing();

        return postRepository.findByUserIdIn(following)
                .orElseThrow(() -> new IllegalStateException("Error retrieving posts for user feed"));
    }

    public List<Post> getAnotherUserFeed(String targetUserId) {
        if (!userRepository.existsById(targetUserId)) {
            throw new IllegalArgumentException("Target user not found");
        }

        return postRepository.findByUserId(targetUserId)
                .orElseThrow(() -> new IllegalStateException("Error retrieving posts for another user's feed"));
    }
}
