package com.proxy.proxyseller.service;

import com.proxy.proxyseller.model.Comment;
import com.proxy.proxyseller.model.Post;
import com.proxy.proxyseller.repository.CommentRepository;
import com.proxy.proxyseller.repository.PostRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Service class for handling operations related to posts.
 * This includes creating, editing, and deleting posts,
 * as well as managing likes and comments on posts.
 */
@Service
public class PostService {

    private final PostRepository postRepository;
    private final CommentRepository commentRepository;

    public PostService(PostRepository postRepository, CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }

    public Post createPost(Post post) {
        if (post.getContent() == null || post.getContent().isEmpty()) {
            throw new IllegalArgumentException("Post content cannot be empty");
        }

        post.setLikes(new ArrayList<>());
        post.setComments(new ArrayList<>());
        post.setCreatedAt(LocalDateTime.now());
        return postRepository.save(post);
    }

    public Post editPost(String postId, Post updatedPost) {
        return postRepository.findById(postId).map(post -> {
            if (updatedPost.getContent() != null && !updatedPost.getContent().isEmpty()) {
                post.setContent(updatedPost.getContent());
            }
            return postRepository.save(post);
        }).orElseThrow(() -> new IllegalArgumentException("Post not found"));
    }

    public void deletePost(String postId) {
        if (!postRepository.existsById(postId)) {
            throw new IllegalArgumentException("Post not found");
        }
        postRepository.deleteById(postId);
    }

    public void likePost(String postId, String userId) {

        Optional<Post> post = postRepository.findById(postId);
        if (post.isPresent()) {
            Post existingPost = post.get();
            if (!existingPost.getLikes().contains(userId)) {
                existingPost.getLikes().add(userId);
                postRepository.save(existingPost);
            } else {
                throw new IllegalStateException("User already liked this post");
            }
        } else {
            throw new IllegalArgumentException("Post not found");
        }
    }

    public void unlikePost(String postId, String userId) {

        Optional<Post> post = postRepository.findById(postId);
        if (post.isPresent()) {
            Post existingPost = post.get();
            if (existingPost.getLikes().contains(userId)) {
                existingPost.getLikes().remove(userId);
                postRepository.save(existingPost);
            } else {
                throw new IllegalStateException("User did not like this post");
            }
        } else {
            throw new IllegalArgumentException("Post not found");
        }
    }

    public Comment addComment(String postId, Comment comment) {
        if (comment.getContent() == null || comment.getContent().isEmpty()) {
            throw new IllegalArgumentException("Comment content cannot be empty");
        }
        comment.setCreatedAt(LocalDateTime.now());
        Comment savedComment = commentRepository.save(comment);

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new IllegalArgumentException("Post not found"));
        post.getComments().add(savedComment);
        postRepository.save(post);

        return savedComment;
    }

    public void deleteComment(String postId, String commentId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new IllegalArgumentException("Post not found"));

        if (post.getComments().removeIf(comment -> comment.getId().equals(commentId))) {
            postRepository.save(post);
            commentRepository.deleteById(commentId);
        } else {
            throw new IllegalArgumentException("Comment not found");
        }
    }

}
